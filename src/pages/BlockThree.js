import React from "react";
import "../App.css";
import picto5 from "../assets/picto5.png";
import picto6 from "../assets/picto6.png";
import picto7 from "../assets/picto7.png";
import { Container, Row, Col } from "react-bootstrap";

const BlockThree = () => {
  return (
    <div className="BlockThree">
      <div className="BlockThree-background">
        <Container>
          <h1 className="BlockThree-title">
            Des activités pour tous les goûts
          </h1>
          <Row>
            <Col>
              <img src={picto5} alt="picto5" className="BlockThree-picto5" />
              <h2 className="BlockThree-subtitle">De la découverte</h2>
              <p className="BlockThree-subtitle2">
                Pour rythmer vos journée, nous vous proposons différentes
                activités de découverte en passant des visites aux concerts
              </p>
            </Col>
            <Col>
              <img src={picto6} alt="picto6" className="BlockThree-picto6" />
              <h2 className="BlockThree-subtitle">Du sport</h2>
              <p className="BlockThree-subtitle2">
                Pour les plus actifs d’entre vous, retrouvez un grand choix de
                sport collectif ou individuel
              </p>
            </Col>
            <Col>
              <img src={picto7} alt="picto7" className="BlockThree-picto7" />
              <h2 className="BlockThree-subtitle">Des cours</h2>
              <p className="BlockThree-subtitle2">
                Envie d’apprendre de nouvelles choses ? Découvrez notre section
                “Découverte” afin de monter en compétence
              </p>
            </Col>
          </Row>
        </Container>
      </div>
      <div className="BlockThree-video">
        <video controls width="800">
          <source
            src="https://vadrouille-landing-storage-48a35800135811-staging.s3.amazonaws.com/Sequence_01.mp4"
            type="video/mp4"
          />
        </video>
      </div>
    </div>
  );
};

export default BlockThree;
