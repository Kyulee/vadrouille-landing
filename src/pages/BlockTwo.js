import React from "react";
import "../App.css";
import picto2 from "../assets/picto2.png";
import picto3 from "../assets/picto3.png";
import picto4 from "../assets/picto4.png";
import { Container, Row, Col } from "react-bootstrap";

const BlockTwo = () => {
  return (
    <div className="BlockTwo">
      <Container>
        <h1 className="BlockTwo-title">Des avantages exclusifs</h1>
        <Row>
          <Col>
            <img src={picto2} alt="picto2" className="BlockTwo-picto2" />
            <h2 className="BlockTwo-subtitle">
              Des partenaires de votre région
            </h2>
            <p>
              Trouvez des activités dans les associations et entreprises près de
              chez vous et depuis chez vous{" "}
            </p>
          </Col>
          <Col>
            <img src={picto3} alt="picto3" className="BlockTwo-picto3" />
            <h2 className="BlockTwo-subtitle">Des prix réduits</h2>
            <p>
              Votre statut de retraité vous donne accès à des remises
              exceptionnelles afin d’occuper vos journées tout en faisant des
              économies
            </p>
          </Col>
          <Col>
            <img src={picto4} alt="picto4" className="BlockTwo-picto4" />
            <h2 className="BlockTwo-subtitle">Seul ou à plusieurs</h2>
            <p>
              Vous avez la possibilité de faire vos activités seul ou à
              plusieurs afin de faire de nouvelles rencontres
            </p>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default BlockTwo;
