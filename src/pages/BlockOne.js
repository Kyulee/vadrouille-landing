import React from "react";
import "../App.css";
import logo from "../assets/logo512.png";
import picto1 from "../assets/picto1.png";
import { Button, Container, Form, Row, Col } from "react-bootstrap";

const BlockOne = () => {
  return (
    <div className="BlockOne">
      <Container>
        <header className="App-header">
          <img src={logo} alt="logo" className="App-logo" />
        </header>
        <Row>
          <Col>
            <img src={picto1} alt="picto1" className="App-picto1" />
          </Col>
          <Col>
            <h1 className="App-title">
              La Vadrouille :
              <h1 className="BlockOne-title">passez une retraite animée !</h1>
            </h1>

            <p className="BlockOne-subtitle">
              Découvrez La Vadrouille, la première application mobile déstinée
              aux activités des retraités
            </p>
            <Form>
              <Row>
                <Col>
                  <Form.Control placeholder="Email" aria-label="Email" />
                </Col>
                <Col>
                  <Button className="button-color">Je m'inscris</Button>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default BlockOne;
