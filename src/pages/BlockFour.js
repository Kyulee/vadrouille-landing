import React from "react";
import "../App.css";
import testmockup from "../assets/testmockup.png";
import { Button, Container, Form, Row, Col } from "react-bootstrap";

const BlockFour = () => {
  const openInNewTab = (url) => {
    window.open(url, "_blank", "noopener,noreferrer");
  };
  return (
    <div className="BlockOne">
      <Container>
        <Row>
          <Col>
            <Row>
              <h2 className="BlockFour-subtext1">
                L’application{" "}
                <b className="BlockFour-subtext2-1">La Vadrouille</b> est
                bientôt disponible, <br />
                inscrivez-vous vite pour être notifié de sa sortie !
              </h2>
              <Form>
                <Row>
                  <Col>
                    <Form.Control placeholder="Email" aria-label="Email" />
                  </Col>
                  <Col>
                    <Button className="button-color">Je m'inscris</Button>
                  </Col>
                </Row>
              </Form>
            </Row>
            <Row>
              <h2 className="BlockFour-subtext2">
                En attendant, vous pouvez nous aider en répondant à notre
                <b className="BlockFour-subtext2-1">questionnaire</b> en ligne
              </h2>
              <Button
                className="BlockFour-buttonQ"
                onClick={() =>
                  openInNewTab(
                    "https://docs.google.com/forms/d/182F8EW9rw-zr-3H8n_fSnGNaCuudgk5rLT5kW1Vmh44/prefill"
                  )
                }
              >
                Le questionnaire
              </Button>
            </Row>
          </Col>
          <Col>
            <img src={testmockup} alt="testmockup" className="testmockup" />
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default BlockFour;
