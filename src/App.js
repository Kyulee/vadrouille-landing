import "./App.css";
import BlockOne from "./pages/BlockOne";
import "bootstrap/dist/css/bootstrap.min.css";
import BlockTwo from "./pages/BlockTwo";
import BlockThree from "./pages/BlockThree";
import BlockFour from "./pages/BlockFour";

function App() {
  return (
    <div className="App">
      <BlockOne />
      <BlockTwo />
      <BlockThree />
      <BlockFour />
    </div>
  );
}

export default App;
